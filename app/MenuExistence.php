<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuExistence extends Model
{
    protected $table = 'wp_postmeta';
    protected $primaryKey = 'meta_id';
    public $timestamps = false;

    const KEY = 'td_mega_menu_cat';

    protected $hidden = [
        'meta_key', 'meta_id', 'post_id'
    ];

    public function newQuery()
    {
        return parent::newQuery()->whereMetaKey(static::KEY)->whereNotNull('meta_value');
    }

    public function categories(){
        return $this->hasMany( self::class, 'parent', 'term_id')->ordered();
    }
}
