<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trend extends Model
{
    protected $table = 'wp_postmeta';
    protected $primaryKey = 'meta_id';
    public $timestamps = false;

    const KEY = 'post_views_count_7_day_total';

    protected $hidden = [
        'meta_key', 'meta_id', 'post_id'
    ];

    public function newQuery()
    {
        return parent::newQuery()->whereMetaKey(static::KEY);
    }
}
