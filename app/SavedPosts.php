<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SavedPosts extends Model
{
    protected $table = 'wp_saved_posts';

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
