<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    protected $table = 'wp_terms';
    protected $primaryKey = 'term_id';
    public $timestamps = false;

    protected $hidden = [
        'term_group'
    ];

    public function items(){
        return $this->belongsToMany(MenuItem::class, 'wp_term_relationships',
            'term_taxonomy_id', 'object_id')->with('icon')
            ->has('existence')
            ->with('existence')
            ->ordered();
    }
}
