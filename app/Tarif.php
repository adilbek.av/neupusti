<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class   Tarif extends Model
{
    protected $table = 'wp_posts';
    protected $primaryKey = 'ID';
    public $timestamps = true;
    const CREATED_AT = 'post_date';
    const UPDATED_AT = 'post_modified';
    const STATUS = 'publish';
    const TYPE = 'product';

    protected $hidden = [
        'post_status', 'post_date_gmt',
        'post_modified_gmt', 'ping_status',
        'comment_status', 'post_mime_type', 'guid',
        'post_excerpt', 'post_password',
        'to_ping', 'pinged',
        'post_content_filtered', 'post_parent',
        'menu_order', 'post_author',
        'post_type', 'post_modified',
        'payment', 'replace',
        'view', 'post_content', 'cover', 'video'
    ];

    protected $attributes = [
        'post_status' => 'publish',
        'ping_status' => 'open',
        'post_type' => 'product',
    ];

    protected $appends = ['price', 'level'];

    public function newQuery()
    {
        return parent::newQuery()->wherePostType(static::TYPE)->wherePostStatus(static::STATUS);
    }

    public function cost()
    {
        return $this->hasOne(Price::class, 'post_id');
    }

    public function meta_level()
    {
        return $this->hasOne(Level::class, 'post_id');
    }

    public function getPriceAttribute() : int {
        return $this->cost ? $this->cost->meta_value : 0;
    }

    public function getLevelAttribute() : int {
        return $this->meta_level ? $this->meta_level->meta_value : '';
    }
}
