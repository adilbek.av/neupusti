<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Video extends Model
{
    protected $table = 'wp_postmeta';
    protected $primaryKey = 'meta_id';
    public $timestamps = false;

    const KEY = '%oembed%';
    const EXCEPT = '%oembed_time%';

    protected $hidden = [
        'meta_key', 'meta_id',
    ];

    protected $appends = ['src'];

    public function newQuery()
    {
        return parent::newQuery()
            ->where('meta_key', 'LIKE', static::KEY)
            ->where('meta_key', 'NOT LIKE', static::EXCEPT);
    }

    public function getSrcAttribute() {

        preg_match('/src="([^"]+)"/', $this->meta_value, $match);
        return $match ? $match[1] : null;
    }
}
