<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Mail;

class EmailController extends Controller
{
    public function sendFeedback () {

        request()->validate([
            'name' => 'required|string',
            'subject' => 'required|string|max:255',
            'message' => 'required',
            'email' => 'required|string|email|max:255'
        ]);

        Mail::send('emails.feedback', ['msg' => request('message')], function ($message)
        {
            $message->from(request('email'), request('name'));
            $message->to('info@neupusti.net');
            $message->subject(request('subject'));

        });
        return response()->json(['message' => trans('messages.emails.success')], 200);
    }
}
