<?php

namespace App\Http\Controllers;

use App\Device;
use App\NotificationList;
use ExponentPhpSDK\Expo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DeviceController extends Controller
{
    public function registerDevice() {
        $validator = Validator::make(request()->all(), [
            'device'    =>  'required|string',
        ]);

        $token = request('device');
        $interestDetails = [$token, 'ExponentPushToken[' .$token .']'];
        $expo = Expo::normalSetup();

        // Subscribe the recipient to the server
        $expo->subscribe($interestDetails[0], $interestDetails[1]);

        Device::create([
            'device' => request('device')
        ]);

        return 'OK';
    }

    public function nots() {

        $nots = NotificationList::whereUserId(1022)->latest()->paginate(request('per_page'), ['*'], 'page', request('page'));
        return response()->json($nots);
    }
}
