<?php

namespace App\Http\Controllers;

use App\Avatar;
use App\Device;
use App\SocialAccount;
use App\User;
use Carbon\Carbon;
use Keygen\Keygen;
use MikeMcLin\WpPassword\Facades\WpPassword;
use Tymon\JWTAuth\Facades\JWTAuth;
use Laravel\Socialite\Facades\Socialite;
use Intervention\Image\Facades\Image;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt', [
            'only' => [
                'edit', 'me', 'logout', 'refresh', 'changeAvatar', 'changePassword'
            ]
        ]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        request()->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $user = User::whereUserEmail(request('email'))
            ->orWhere('user_login', '=', request('email'))
            ->first();
        if ($user && WpPassword::check(request('password'), $user->user_pass) && $token = JWTAuth::fromUser($user)) {
            if (request('device')) {
                Device::whereUserId($user->ID)->delete();
                Device::whereDevice(request('device'))->update(['user_id' => $user->ID]);
                $user->update(['device_id' => request('device')]);
            }
            return $this->respondWithToken($token);
        }

        return response()->json(['message' => 'The given data was invalid.',
            'errors' => [
                'password' => ['Email/Логин и пароль не соответствуют друг другу']
            ]], 401);
    }

    /**
     * Create a new user instance with a valid registration.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register()
    {

        request()->validate([
            'username' => 'required|string|max:255|unique:wp_users,user_login',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:wp_users,user_email',
            'password' => 'required|string|min:6|confirmed',
            'gender' => 'required|in:Жен,Муж',
            'device' => 'nullable'
        ]);

        $nickname = str_replace('.', '-', request('username'));
        $nickname = str_replace(' ', '-', $nickname);
        $nickname = trim(mb_strtolower($nickname));

        $user = User::create([
            'user_login' => request('username'),
            'display_name' => request('first_name') . ' ' . request('last_name'),
            'user_email' => request('email'),
            'user_pass' => WpPassword::make(request('password')),
            'user_nicename' => $nickname,
            'user_registered' => Carbon::now()
        ]);

        if (request()->filled('device')) {
            Device::whereDevice(request('device'))->update([
                'user_id' => $user->ID
            ]);
        }

        $user->gender()->create(
            [
                'meta_key' => 'gender',
                'meta_value' => request('gender')
            ]
        );

        if (request()->get('country')) {
            $user->country()->create(
                [
                    'meta_key' => 'country',
                    'meta_value' => request('country')
                ]
            );
        }

        if ($token = JWTAuth::fromUser($user)) {
            return $this->respondWithToken($token);
        }

        return response()->json(['error' => trans('auth.unauthorized')], 401);

    }

    /**
     * Create a new user instance with a valid registration.
     *
     * @return \Illuminate\Http\JsonResponse
     */

    public function edit() {

        $user = auth()->user();

        request()->validate([
            'username' => 'required|string|max:255|unique:wp_users,user_login,'.$user->ID.',ID',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:wp_users,user_email,'.$user->ID.',ID',
            'avatar' => 'nullable|image|max:200'
        ]);

        $nickname = str_replace('.', '-', request('username'));
        $nickname = str_replace(' ', '-', $nickname);
        $nickname = trim(mb_strtolower($nickname));

        $user->update([
            'user_login' => request('username'),
            'display_name' => request('first_name') . ' ' . request('last_name'),
            'user_email' => request('email'),
            'user_nicename' => $nickname,
        ]);

        return response()->json($user);
    }

    public function changePassword() {
        request()->validate([
            'old_password' => 'required|string|min:6',
            'new_password' => 'required|string|min:6',
        ]);

        if (WpPassword::check(request('old_password'), auth()->user()->user_pass)) {
            auth()->user()->update([
                'user_pass' => WpPassword::make(request('new_password'))
            ]);

            return response()->json(['success' => true, 'error' => false]);
        }

        return response()->json(['success' => false, 'error' => true]);

    }

    public function changeAvatar() {
        request()->validate([
            'avatar' => 'required'
        ]);

//        return auth()->user()->ID;
        $hash = time() . Keygen::alphanum(8)->generate();
        $folder = 'uploads/';
        $path = $folder . $hash . '.png';
        Image::make(request('avatar'))->save(public_path($path), 90);


        Avatar::updateOrCreate([
            'post_author' => auth()->user()->ID,
            'post_type' => 'attachment',
            'post_status' => 'inherit',
            'ping_status' => 'closed',
        ], [
            'post_mime_type' => 'image/png',
            'comment_count' => 0,
            'menu_order' => 0,
            'guid' => asset($path),
            'post_parent' => 0,
            'post_title' => $hash,
            'post_name' => $hash,
            'post_excerpt' => '',
            'to_ping' => '',
            'pinged' => '',
            'post_content_filtered' => '',
            'post_content' => '',
            'post_modified_gmt' => Carbon::now(),
            'post_date_gmt' => Carbon::now(),
        ]);
        return response()->json(['message' => trans('auth.avatar'), 'avatar' => asset($path)]);
    }


    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        $user = auth()->user()->load(['avatar', 'level']);
        return response()->json($user);
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {

        $user = auth()->user();

        Device::whereUserId($user->ID)->whereDevice(request('device'))->update([
            'user_id' => null
        ]);

        auth()->logout();

        return response()->json(['message' => trans('auth.logged_out')]);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 6000
        ]);
    }

    /**
     * Redirect the user to the OAuth Provider.
     *
     * @return \Illuminate\Http\Response
     */
    // Social Authentication
    public function redirect($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function callback($provider)
    {

        if (!request()->has('code') || request()->has('error')) {
            return '';
        }
        $user = Socialite::driver($provider)->user();

        if ($user) {
           return $this->findOrCreateUser($user, $provider);
        }

        return '';

    }

    public function oauth($provider)
    {
        request()->validate([
            'id' => 'required',
            'email' => 'required|email',
            'device' => 'nullable',
            'name' => 'required'
        ]);

        return $this->findOrCreateUser(request(), $provider);
    }

    public function findOrCreateUser($user, $provider)
    {
        $identity = null;

        if ($provider == 'facebook') {
            $identity = 'https://www.facebook.com/app_scoped_user_id/' . $user->id .'/';
        } elseif ($provider == 'google') {
            $identity = 'https://plus_google_com/u/0/' . $user->id .'/';
        } elseif ($provider == 'vkontakte') {
            if ($user && $token = JWTAuth::fromUser(User::whereUserEmail($user->accessTokenResponseBody['email'])->first())) {
                return redirect()->route('token', ['provider' => 'vkontakte', 'token' => $token]);
            }
        }
        $authUser = SocialAccount::where('network', $provider)
            ->where('identity', $identity)
            ->with('user')
            ->first();

        if ($authUser && $token = JWTAuth::fromUser($authUser->user)) {
            return $this->respondWithToken($token);
        }

        $newUser = User::firstOrCreate([
            'user_email'    => $user->email
        ],[
            'display_name'     => $user->name,
            'user_email'    => $user->email,
            'user_login'    => $user->email,
            'user_nicename'    => $user->email,
            'user_registered' => Carbon::now()
        ]);

        Device::whereDevice(request('device'))->update([
            'user_id' => $newUser->ID
        ]);

        $account = new SocialAccount();
        $account->network = $provider;
        $account->identity = $user->id;

        $newUser->accounts()->save($account);

        if ($token = JWTAuth::fromUser($newUser)) {
            return $this->respondWithToken($token);
        }
    }
}