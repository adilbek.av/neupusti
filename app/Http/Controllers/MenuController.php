<?php

namespace App\Http\Controllers;

use App\Menu;
use App\Term;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MenuController extends Controller
{

    public function index()
    {
        $menus = Menu::with(['title', 'items'])->ordered()->get();

        return response()->json($menus);
    }

    public function show($slug)
    {
        $menu = Term::whereSlug($slug)->with('items')->first();

        return response()->json($menu);
    }
}
