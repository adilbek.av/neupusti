<?php

namespace App\Http\Controllers;

use App\Tarif;

class TarifController extends Controller
{
    public function index() {
        return Tarif::get();
    }

    public function showTarif($level) {
        return Tarif::whereHas('meta_level', function ($query) use ($level) {
            $query->where('meta_value', $level);
        })->first();
    }

    public function show() {
        $tarif = Tarif::where('ID', '=', request('id'))->first();
        return view('oplata', compact('tarif'));
    }
}
