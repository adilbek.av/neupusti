<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryName;
use App\Country;
use App\Notification;
use App\NotificationList;
use App\Post;
use App\SavedPosts;
use ExponentPhpSDK\Expo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PostController extends Controller
{
    /**
     * Create a new PostController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('jwt', [
            'only' => ['update', 'addToNotification']
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $countries = null;
        if(request('country')) {
            $country = Category::whereSlug('countries')->ordered()->first()->children->where('slug', $this->transliterate(request('country')));
            if (count($country)) {
                $countries = $country->pluck('term_id');
            }
        }
        $posts = Post::when(request('expand'), function ($query) {
            $query->with(explode(',', trim(request('expand'), '[]')));
        })

//        with(['video', 'user', 'deadline', 'categories'])
            ->categories($countries)
            ->categories(request('categories'))
            ->search(request('q'))
            ->saved(request('saved'))
            ->premium(request('premium'))
            ->ordered(request('sort'))
            ->paginate(request('per_page'), ['*'], 'page', request('page'));

        return response()->json($posts);
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $post = $post->load(['video', 'user', 'categories', 'deadline']);
        return response()->json($post);
    }

    /**
     * Attach/Detach saved Post.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Post $post)
    {
        $post->savings()->toggle(auth()->user()->ID);

        return response()->json(['messages' => [
            'saved' => trans('messages.posts.attached'),
            'removed' => trans('messages.posts.detached')
        ]], 201);
    }

    public function addToNotification(Post $post)
    {
        $post->notifications()->syncWithoutDetaching(auth()->user()->ID);

        return response()->json(['messages' => trans('messages.posts.notified')], 201);
    }

    public function transliterate($text) {
        $cyrCountries = [
            'Азербайджан','Армения', 'Беларусь', 'Грузия', 'Казахстан', 'Киргизия', 'Молдова', 'Россия', 'Таджикистан',
            'Туркменистан', 'Узбекистан', 'Украина'
        ];
        $latCountries = [
            'azerbajdzhan','armenia', 'belarus', 'georgia', 'kazahstan', 'kyrgyzstan', 'moldova', 'russia', 'tadzhikistan',
            'turkmenistan', 'uzbekistan', 'ukraine'
        ];

        return str_replace($cyrCountries, $latCountries, $text);
    }

    public function notify() {


        $dPosts = Post::orderBy('post_date', 'DESC')
            ->has('deadline')->join('wp_postmeta', 'wp_posts.ID', '=', 'wp_postmeta.post_id')
            ->where('wp_postmeta.meta_value', '>', time() + (24*60*60))
            ->where('wp_postmeta.meta_value', '<=', (time() + (24*60*60+60)))
            ->with('deadline')
            ->get()->pluck('post_title', 'ID');

        $dPosts3 = Post::orderBy('post_date', 'DESC')
            ->has('deadline')->join('wp_postmeta', 'wp_posts.ID', '=', 'wp_postmeta.post_id')
            ->where('wp_postmeta.meta_value', '>', time() + (3*24*60*60))
            ->where('wp_postmeta.meta_value', '<=', (time() + (3*24*60*60+60)))
            ->with('deadline')
            ->get()->pluck('post_title', 'ID');

        $wPosts = Post::orderBy('post_date', 'DESC')
            ->has('deadline')->join('wp_postmeta', 'wp_posts.ID', '=', 'wp_postmeta.post_id')
            ->where('wp_postmeta.meta_value', '>', time() - (7*24*60*60))
            ->where('wp_postmeta.meta_value', '<=', (time() + (7*24*60*60+60)))
            ->with('deadline')
            ->get()->pluck('post_title', 'ID');


        foreach ($dPosts as $id=>$title) {
            $wNotificationDevices = Notification::wherePostId($id)->with(['user'=>function($query){
                $query->with(['device']);
            }])->get()->pluck('user.device');

            $wSavedDevices = SavedPosts::wherePostId($id)->with(['user'=>function($query){
                $query->with(['device']);
            }])->get()->pluck('user.device');
            $wDevices = $wNotificationDevices->merge($wSavedDevices);
            if ($wDevices && $wDevices->count() && $wDevices[0]) {
                $expo = Expo::normalSetup();

                $notification = ['body' => 'У вас осталось 1 день для подачи заявки.', 'title' => $title];

                foreach ($wDevices->pluck('user_id')->unique() as $userID) {
                    NotificationList::create([
                        'user_id' => $userID,
                        'title' => $notification['title'],
                        'body' => $notification['body']
                    ]);
                }

                $expo->notify($wDevices->pluck('device')->unique()->toArray(), $notification);
            }
        }

        foreach ($dPosts3 as $id=>$title) {
            $wNotificationDevices = Notification::wherePostId($id)->with(['user'=>function($query){
                $query->with(['device']);
            }])->get()->pluck('user.device');

            $wSavedDevices = SavedPosts::wherePostId($id)->with(['user'=>function($query){
                $query->with(['device']);
            }])->get()->pluck('user.device');
            $wDevices = $wNotificationDevices->merge($wSavedDevices);
            if ($wDevices && $wDevices->count() && $wDevices[0]) {
                $expo = Expo::normalSetup();

                $notification = ['body' => 'У вас осталось 3 дня для подачи заявки.', 'title' => $title];

                foreach ($wDevices->pluck('user_id')->unique() as $userID) {
                    NotificationList::create([
                        'user_id' => $userID,
                        'title' => $notification['title'],
                        'body' => $notification['body']
                    ]);
                }

                $expo->notify($wDevices->pluck('device')->unique()->toArray(), $notification);
            }
        }

        foreach ($wPosts as $id=>$title) {
            $wNotificationDevices = Notification::wherePostId($id)->with(['user'=>function($query){
                $query->with(['device']);
            }])->get()->pluck('user.device');

            $wSavedDevices = SavedPosts::wherePostId($id)->with(['user'=>function($query){
                $query->with(['device']);
            }])->get()->pluck('user.device');

            $wDevices = $wNotificationDevices->merge($wSavedDevices);

            if ($wDevices && $wDevices->count() && $wDevices[0]) {

                $expo = Expo::normalSetup();

                $notification = ['body' => 'У вас осталось 7 дней для подачи заявки.', 'title' => $title];


                foreach ($wDevices->pluck('user_id')->unique() as $userID) {
                    NotificationList::create([
                        'user_id' => $userID,
                        'title' => $notification['title'],
                        'body' => $notification['body']
                    ]);
                }



                $expo->notify($wDevices->pluck('device')->unique()->toArray(), $notification);
            }
        }

        return response()->json(['message' => 'success']);
    }
}
