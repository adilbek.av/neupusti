<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::doesntHave('parent')
            ->with(['title', 'children'])->ordered()->get();

        return response()->json($categories);
    }

    /**
     * Display a listing of countries.
     *
     * @return \Illuminate\Http\Response
     */
    public function countries()
    {
        $countries = Category::whereSlug('countries')->ordered()->first()->children;

        return response()->json($countries);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::whereTermId($id)->when(request('expand'), function ($query) {
            $query->with(explode(',', trim(request('expand'), '[]')));
        })->with(['title', 'children'])->first();

        return response()->json($category);
    }
}
