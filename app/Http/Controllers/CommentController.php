<?php

namespace App\Http\Controllers;

use App\Comment;

class CommentController extends Controller
{

    public function __construct()
    {
        $this->middleware('jwt', [
            'only' => [
                'store'
            ]
        ]);
    }


    public function index()
    {
        $comments = Comment::with(['user'])->orderBy('comment_date', 'DESC')->get();

        return response()->json($comments);
    }

    public function show($id)
    {
        $comments = Comment::with(['user'])->where('comment_post_id', '=', $id)->orderBy('comment_date', 'DESC')->get();

        return response()->json($comments);
    }

    public function store()
    {

        request()->validate([
            'comment_post_id' => 'required|integer',
            'comment_content' => 'required'
        ]);

        request()->request->add([
            'user_id' => auth()->user()->ID,
            'comment_parent' => 0,
            'comment_type' => 'post',
            'comment_author' => auth()->user()->display_name,
            'comment_author_email' => auth()->user()->user_email
        ]);
        $comment = Comment::create(request()->all());
        return response()->json($comment->load('user'));
    }
}
