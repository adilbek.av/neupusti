<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Foundation\Auth\ResetsPasswords;
use MikeMcLin\WpPassword\Facades\WpPassword;

class ResetPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reset()
    {
        request()->validate([
            'email' => 'required|email',
            'code' => 'required|digits:5',
            'password' => 'required|string|min:6|confirmed',
        ]);

        if (request()->wantsJson()) {
            $user = User::where('user_email', request('email'))->first();
            if ($user && WpPassword::check(request('code'), $user->user_activation_key)) {
                $user->update([
                    'user_pass' => WpPassword::make(request('password')),
                    'user_activation_key' => ''
                ]);
                return response()->json(['message' => trans('passwords.reset')], 200);
            }
            return response()->json(['error' => trans('passwords.no_reset')], 400);
        }
    }
}
