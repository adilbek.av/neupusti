<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Support\Facades\Mail;
use Keygen\Keygen;
use MikeMcLin\WpPassword\Facades\WpPassword;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */


    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Send a reset code to the given user.
     *
     * @return \Illuminate\Http\Response
     */
    public function getResetCode()
    {
        request()->validate(['email' => 'required|email']);
        if (request()->wantsJson()) {
            $user = User::where('user_email', request('email'))->first();
            if (!$user) {
                return response()->json(['error' => trans('passwords.user')], 400);
            }
//            $code = Keygen::alphanum(12)->generate();
//            $user->update(['user_pass' => WpPassword::make($code)]);
//            Mail::send('emails.code', ['code' => $code], function ($message)
//            {
//                $message->from('info@neupusti.net', 'Neupusti.net');
//                $message->to(request('email'));
//                $message->subject("Change password");
//
//            });

            $client = new Client();

            $response = $client->request('POST', 'https://neupusti.net/iump-reset-password-2/', [
                'form_params' => [
                    'ihcaction' => 'reset_pass',
                    'email_or_userlogin' => request('email'),
                    'Submit' => 'Получить новый пароль'
                ]
            ]);
            $response = $response->getBody()->getContents();

            return response()->json(['message' => trans('passwords.sent'), 'email' => request('email')], 200);
        }
    }

    /**
     * Verify a reset code of the given user.
     *
     * @return \Illuminate\Http\Response
     */
    public function verifyResetCode()
    {
        request()->validate([
            'email' => 'required|email',
            'code' => 'required|digits:5'
        ]);
        if (request()->wantsJson()) {
            $user = User::where('user_email', request('email'))->first();
            if ($user && WpPassword::check(request('code'), $user->user_activation_key)) {
                return response()->json(['message' => true, 'email' => request('email'), 'code' => request('code')], 200);
            }
            return response()->json(['error' => trans('passwords.token')], 400);
        }
    }



}
