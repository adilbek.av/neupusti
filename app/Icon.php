<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Icon extends Model
{
    protected $table = 'wp_postmeta';
    protected $primaryKey = 'meta_id';
    public $timestamps = false;

    const KEY = 'menu-icons';

    protected $hidden = [
        'meta_key', 'meta_id', 'post_id', 'meta_value'
    ];

    protected $appends = ['src'];

    public function newQuery()
    {
        return parent::newQuery()->whereMetaKey(static::KEY);
    }

    public function getSrcAttribute() {
        return unserialize($this->meta_value)['icon'];
    }
}
