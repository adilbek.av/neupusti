<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryName extends Model
{
    protected $table = 'wp_terms';
    protected $primaryKey = 'term_id';
    public $timestamps = false;

    protected $hidden = [
        'term_group'
    ];

    public function getNameAttribute($value) {
        return $this->info->parent === 107 ? $this->transliterate($this->slug): $value;
    }

    public function info(){
        return $this->belongsTo( Category::class, 'term_id');
    }

    public function transliterate($text) {
        $cyrCountries = [
            'Азербайджан','Армения', 'Беларусь', 'Грузия', 'Казахстан', 'Киргизия', 'Молдова', 'Россия', 'Таджикистан',
            'Туркменистан', 'Узбекистан', 'Украина'
        ];
        $latCountries = [
            'azerbajdzhan','armenia', 'belarus', 'georgia', 'kazahstan', 'kyrgyzstan', 'moldova', 'russia', 'tadzhikistan',
            'turkmenistan', 'uzbekistan', 'ukraine'
        ];

        return str_replace($latCountries, $cyrCountries, $text);
    }
}
