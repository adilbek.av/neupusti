<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'wp_comments';
    protected $primaryKey = 'comment_id';
    public $timestamps = false;
    const CREATED_AT = 'comment_date';

    const TYPE = 'post';

    protected $hidden = [
        'comment_author_url', 'comment_author_IP',
        'comment_karma', 'comment_date_gmt', 'comment_agent', 'comment_parent', 'user_id'
    ];
    protected $fillable = [
        'comment_post_id', 'comment_date', 'comment_date_gmt',
        'comment_content', 'comment_parent', 'user_id', 'comment_type',
        'comment_author', 'comment_author_email'
        ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $model->comment_date = $model->freshTimestamp();
            $model->comment_date_gmt = $model->freshTimestamp();
        });
    }

    public function newQuery()
    {
        return parent::newQuery()->whereCommentType(static::TYPE);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'ID')->with('avatar');
    }
}
