<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use League\HTMLToMarkdown\HtmlConverter;

class Post extends Model
{
    protected $table = 'wp_posts';
    protected $primaryKey = 'ID';
    public $timestamps = true;
    const CREATED_AT = 'post_date';
    const UPDATED_AT = 'post_modified';
    const STATUS = 'publish';
    const TYPE = 'post';

    protected $hidden = [
        'post_status', 'post_date_gmt',
        'post_modified_gmt', 'ping_status',
        'comment_status', 'post_mime_type', 'guid',
        'post_excerpt', 'post_password',
        'to_ping', 'pinged',
        'post_content_filtered', 'post_parent',
        'menu_order', 'post_author',
        'post_type', 'post_modified',
        'payment', 'replace',
        'view', 'post_content', 'cover', 'video'
    ];

    protected $attributes = [
        'post_status' => 'publish',
        'ping_status' => 'open',
        'post_type' => 'post',
    ];

    protected $appends = [
        'content', 'view_count', 'image', 'src', 'premium', 'isSaved'
    ];

    public function newQuery()
    {
        return parent::newQuery()->wherePostType(static::TYPE)->wherePostStatus(static::STATUS);
    }

    public function getIsSavedAttribute()
    {
        if (auth()->user()) {
            $saved = $this->savings()->whereUserId(auth()->user()->ID)->first();
            return (!is_null($saved)) ? true : false;
        }
        return false;
    }

    // Getters

    public function getContentAttribute()
    {
//        $converter = new HtmlConverter();
//
//        if ($this->premium) {
//            if (auth()->user() && auth()->user()->premium) {
//                return $converter->convert($this->post_content);
//            }
//            else {
//                return $converter->convert(explode('[ihc-hide-content', $this->replace->meta_value)[0]);
//            }
//        } else {
//            return $converter->convert($this->post_content);
//        }

        if ($this->premium) {
            if (auth()->user() && auth()->user()->premium) {
                return $this->post_content;
            }
            else {
                return explode('[ihc-hide-content', $this->replace->meta_value)[0];
            }
        } else {
            return $this->post_content;
        }
    }

    /*
     * Relations
     */

    public function savings() {
        return $this->belongsToMany(User::class, 'wp_saved_posts',
            'post_id', 'user_id', 'ID', 'ID');
    }

    public function notifications() {
        return $this->belongsToMany(User::class, 'wp_notified_posts',
            'post_id', 'user_id', 'ID', 'ID');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'post_author', 'ID');
    }

    public function cover()
    {
        return $this->hasOne(Cover::class, 'post_parent');
    }

    public function thumbnail()
    {
        return $this->hasOne(Thumbnail::class, 'post_id');
    }

    public function getImageAttribute() {
        if ($this->cover) {
            return $this->cover->guid;
        } else if ($this->thumbnail && $this->thumbnail->cover){
            return $this->thumbnail->cover->guid;
        }
        return null;
    }

    public function deadline()
    {
        return $this->hasOne(Deadline::class, 'post_id');
    }

    public function trend()
    {
        return $this->hasOne(Trend::class, 'post_id');
    }

    public function view()
    {
        return $this->hasOne(View::class, 'post_id');
    }

    public function getViewCountAttribute() {
        return $this->view ? $this->view->meta_value : null;
    }

    public function video()
    {
        return $this->hasOne(Video::class, 'post_id');
    }

    public function getSrcAttribute() {
        return $this->video ? $this->video->src : null;
    }

    public function categories(){
        return $this->belongsToMany(Category::class, 'wp_term_relationships',
            'object_id', 'term_taxonomy_id', 'ID', 'term_taxonomy_id')
            ->with(['parent'])->ordered();
    }

    public function payment()
    {
        return $this->hasOne(Premium::class, 'post_id');
    }

    public function getPremiumAttribute() : bool {
        return $this->payment ? $this->payment->meta_value : false;
    }

    public function replace()
    {
        return $this->hasOne(ReplaceContent::class, 'post_id');
    }

    /*
     * Scopes
     */

    public function scopeOrdered($query, $filter = null) {
        if ($filter) {

            if (strpos($filter, '|')) {
                list($field, $order) = explode('|', $filter);

                if ($field == 'deadline') {
                    return $query->has('deadline')->join('wp_postmeta', $this->getTable() . '.ID', '=', 'wp_postmeta.post_id')
                        ->where('wp_postmeta.meta_value', '>', time())
                        ->orderBy('wp_postmeta.meta_value', $order);
                }

                if ($field == 'trend') {
                    return $query->has('trend')->join('wp_postmeta', $this->getTable() . '.ID', '=', 'wp_postmeta.post_id')
                        ->where('wp_posts.post_date', '>', Carbon::now()->subWeek(1))
                        ->orderBy('wp_postmeta.meta_value', $order);
                }

                return $query->orderBy($field, $order);
            }

            if ($filter == 'deadline') {
                return $query->has('deadline')->join('wp_postmeta', $this->getTable() . '.ID', '=', 'wp_postmeta.post_id')
                    ->where('wp_postmeta.meta_value', '>', time())
                    ->orderBy('wp_postmeta.meta_value', 'DESC');
            }

            if ($filter == 'trend') {
                return $query->has('trend')->join('wp_postmeta', $this->getTable() . '.ID', '=', 'wp_postmeta.post_id')
                    ->where('wp_posts.post_date', '>', Carbon::now()->subWeek(1))
                    ->orderBy('wp_postmeta.meta_value', 'DESC');
            }

            return $query->orderBy($filter, 'DESC');
        }

        return $query->orderBy('post_date', 'DESC');
    }

    public function scopeCategories($query, $categories = null) {
        $categories = json_decode($categories);

        return $query->when($categories, function ($query) use ($categories) {
            return $query->whereHas('categories', function ($query) use ($categories) {
                $query->whereIn('term_id', $categories);
            });
        });
    }

    public function scopeSaved($query, $saved = null) {
        $user = auth()->user();
        return $query->when($saved && $user, function ($query) use ($user){
            return $query->whereHas('savings', function ($query) use ($user) {
                $query->where('ID', '=', $user->ID);
            });
        });
    }

    public function scopePremium($query, $premium = null) {
        return $query->when($premium, function ($query) {
            return $query->has('payment');
        });
    }

    public function scopeSearch($query, $q = null) {
        return $query->when($q, function ($query) use ($q) {
            return $query->where(function ($query) use ($q) {
                $query->where('post_title', 'LIKE', '%'.$q.'%')
                    ->orWhere('post_name', 'LIKE', '%'.$q.'%')
                    ->orWhere('post_content', 'LIKE', '%'.$q.'%')
                    ->orWhereHas('categories', function ($query) use ($q) {
                        $query->search($q);
                    });
            });
        });
    }

}
