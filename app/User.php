<?php

namespace App;

use Carbon\Carbon;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $table = 'wp_users';
    protected $primaryKey = 'ID';
    public $timestamps = false;
    const CREATED_AT = 'user_registered';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_login', 'user_email', 'user_pass',
        'user_nicename', 'user_registered', 'user_status',
        'display_name', 'user_activation_key', 'device_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user_pass', 'user_activation_key', 'user_status',
    ];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */

    protected $appends = ['premium'];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function notifications() {
        return $this->belongsToMany(Post::class, 'wp_notified_posts',
            'user_id', 'post_id', 'ID', 'ID');
    }

    public function avatar()
    {
        return $this->hasOne(Avatar::class, 'post_author')->orderByDesc('post_date');
    }

    public function accounts() {
        return $this->hasOne(SocialAccount::class, 'userid');
    }

    public function gender()
    {
        return $this->hasOne(Gender::class, 'user_id');
    }

    public function country()
    {
        return $this->hasOne(Gender::class, 'user_id');
    }

    public function device() {
        return $this->hasOne(Device::class, 'user_id');
    }

    public function level() {
        return $this->hasMany(PremiumAccount::class, 'user_id')->orderByDesc('expire_time')->orderByDesc('start_time');
    }

    public function getPremiumAttribute() {
        if ($this->level->count() && $this->level[0]) {
            return Carbon::parse($this->level[0]->expire_time)->gt(Carbon::now()) || $this->level[0]->expire_time < 1;
        }
        return false;
    }
}
