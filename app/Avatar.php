<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Avatar extends Model
{
    protected $table = 'wp_posts'; //http://neupusti.net/wp-content/uploads/2016/10/dalmira7-1.jpg
    protected $primaryKey = 'ID';
    public $timestamps = true;
    const CREATED_AT = 'post_date';
    const UPDATED_AT = 'post_modified';
    const TYPE = 'attachment';
    const STATUS = 'inherit';

    protected $fillable = [
        'post_date', 'post_modified',
        'post_date_gmt', 'post_modified_gmt',
        'post_type', 'post_mime_type',
        'post_title', 'post_name',
        'post_status', 'post_author',
        'ping_status', 'post_content', 'post_excerpt',
        'to_ping', 'pinged', 'post_content_filtered', 'guid'
    ];

    protected $hidden = [
        'post_status', 'post_date_gmt',
        'post_modified_gmt', 'ping_status',
        'comment_status', 'post_content',
        'post_excerpt', 'post_password',
        'to_ping', 'pinged',
        'post_content_filtered', 'post_parent',
        'menu_order', 'comment_count',
        'post_author', 'ID', 'post_title', 'post_name',
        'post_type', 'post_date', 'post_modified'
    ];

    protected $attributes = [
        'post_status' => 'inherit',
        'ping_status' => 'closed',
        'post_type' => 'attachment',
        'post_parent' => 0
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'ID', 'post_author');
    }

    public function newQuery()
    {
        return parent::newQuery()
            ->wherePostType(static::TYPE)
            ->wherePostStatus(static::STATUS)
            ->wherePostParent(0);
    }
}
