<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Deadline extends Model
{
    protected $table = 'wp_postmeta';
    protected $primaryKey = 'meta_id';
    public $timestamps = false;

    const KEY = '_expiration-date';

    protected $hidden = [
        'meta_key', 'meta_id', 'post_id'
    ];

    protected $appends = ['date', 'left'];

    public function newQuery()
    {
        return parent::newQuery()->whereMetaKey(static::KEY);
    }

    public function getDateAttribute() {
        return gmdate("Y-m-d H:i:s", $this->meta_value);
    }

    public function getLeftAttribute() {
        return Carbon::parse(gmdate("Y-m-d H:i:s", $this->meta_value))->diffForHumans();
    }

}
