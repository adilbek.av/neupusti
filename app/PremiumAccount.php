<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PremiumAccount extends Model
{
    protected $table = 'wp_ihc_user_levels';
    protected $primaryKey = 'id';
    public $timestamps = false;

    const STATUS = true;

    protected $hidden = [
        'update_time', 'status',
        'notification', 'id', 'user_id'
    ];

    public function newQuery()
    {
        return parent::newQuery()->whereStatus(static::STATUS);
    }

}
