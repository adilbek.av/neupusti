<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'wp_term_taxonomy';
    protected $primaryKey = 'term_id';
    public $timestamps = false;

    protected $hidden = [
        'term_taxonomy_id', 'description', 'title', 'term_group'
    ];

    protected $appends = ['name'];
    const TAXONOMY = 'category';

    public function newQuery()
    {
        return parent::newQuery()->whereTaxonomy(static::TAXONOMY);
    }

    /*
     * Relations
     */

    public function children(){
        return $this->hasMany( self::class, 'parent', 'term_id')->ordered();
    }

    public function parent(){
        return $this->hasOne( self::class, 'term_id', 'parent' );
    }

    public function posts(){
        return $this->belongsToMany(Post::class, 'wp_term_relationships',
            'term_taxonomy_id', 'object_id', 'term_taxonomy_id', 'ID')
            ->with(['video', 'user']);
    }

    public function title(){
        return $this->hasOne( CategoryName::class, 'term_id');
    }

    public function getNameAttribute()
    {
        return $this->title ? $this->title->name : null;
    }

    /*
     * Scopes
     */

    public function scopeOrdered($query){
        return $query->join('wp_terms', $this->getTable() . '.term_id', '=', 'wp_terms.term_id')
            ->orderBy('wp_terms.name', 'ASC');
    }


    public function scopeSearch($query, $q = null) {
        return $query->when($q, function ($query) use ($q) {
            return $query->whereHas('title', function ($query) use ($q) {
                $query->where('name', 'LIKE', '%'.$q.'%');
            });
        });
    }
}
