<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Thumbnail extends Model
{
    protected $table = 'wp_postmeta';
    protected $primaryKey = 'meta_id';
    public $timestamps = false;

    const KEY = '_thumbnail_id';

    protected $hidden = [
        'meta_key', 'meta_id', 'post_id'
    ];

    public function newQuery()
    {
        return parent::newQuery()->whereMetaKey(static::KEY)->with('cover');
    }

    public function cover()
    {
        return $this->hasOne(Cover::class, 'ID', 'meta_value');
    }
}
