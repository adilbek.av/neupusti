<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Level extends Model
{
    protected $table = 'wp_postmeta';
    protected $primaryKey = 'meta_id';
    public $timestamps = false;

    const KEY = 'iump_woo_product_level_relation';

    protected $hidden = [
        'meta_key', 'meta_id', 'post_id'
    ];

    public function newQuery()
    {
        return parent::newQuery()->whereMetaKey(static::KEY);
    }
}
