<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuItem extends Model
{
    protected $table = 'wp_posts';
    protected $primaryKey = 'ID';
    public $timestamps = true;
    const CREATED_AT = 'post_date';
    const UPDATED_AT = 'post_modified';
    const STATUS = 'publish';
    const TYPE = 'nav_menu_item';

    protected $hidden = [
        'post_status', 'post_date_gmt',
        'post_modified_gmt', 'ping_status',
        'comment_status', 'post_mime_type', 'guid',
        'post_excerpt', 'post_password',
        'to_ping', 'pinged',
        'post_content_filtered', 'post_parent',
        'menu_order', 'post_author',
        'post_type', 'post_modified',
        'payment', 'replace',
        'view', 'post_content', 'cover', 'video', 'comment_count'
    ];

    protected $attributes = [
        'post_status' => 'publish',
        'ping_status' => 'open',
        'post_type' => 'post',
    ];

    public function newQuery()
    {
        return parent::newQuery()->wherePostType(static::TYPE)->wherePostStatus(static::STATUS);
    }

    public function scopeOrdered($query){
        return $query->orderBy('menu_order', 'ASC');
    }

    public function icon()
    {
        return $this->hasOne(Icon::class, 'post_id');
    }

    public function existence()
    {
        return $this->hasOne(MenuExistence::class, 'post_id');
    }
}
