<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = 'wp_notified_posts';

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
