<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = 'wp_devices';
    public $timestamps = false;
    public $fillable = ['user_id', 'device'];
}
