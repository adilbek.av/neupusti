<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NotificationList extends Model
{
    protected $table = 'wp_notifications';

    protected $fillable = ['user_id', 'title', 'body'];

    public function user() {
        return $this->belongsTo(User::class, 'user_id');
    }
}
