<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gender extends Model
{
    protected $table = 'wp_usermeta';
    protected $primaryKey = 'umeta_id';
    const KEY = 'gender';
    public $timestamps = false;

    protected $fillable = [
        'meta_key', 'meta_value'
    ];

    public function newQuery()
    {
        return parent::newQuery()
            ->whereMetaKey(static::KEY);
    }
}
