<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SocialAccount extends Model
{
    protected $table = 'wp_ulogin';
    protected $primaryKey = 'ID';
    public $timestamps = false;
    protected $fillable = [
        'network', 'userid', 'identity'
    ];


    public function user() {
        return $this->belongsTo(User::class, 'userid');
    }

    public function setIdentityAttribute($value) {

        if ($this->network == 'facebook') {
            $this->attributes['identity'] = 'https://www.facebook.com/app_scoped_user_id/' . $value .'/';
        } elseif ($this->network == 'google') {
            $this->attributes['identity'] = 'https://plus_google_com/u/0/' . $value .'/';
        } elseif ($this->network == 'vkontakte') {
            $this->attributes['identity'] = 'http://vk_com/' . $value;
        }

//        elseif ($this->network == 'mailru') {
//            $this->attributes['identity'] = 'https://my_mail_ru/bk/' . $value .'/';
//        }
    }
}
