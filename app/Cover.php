<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cover extends Model
{
    protected $table = 'wp_posts';
    protected $primaryKey = 'ID';
    public $timestamps = true;
    const CREATED_AT = 'post_date';
    const UPDATED_AT = 'post_modified';
    const TYPE = 'attachment';
    const STATUS = 'inherit';

    protected $hidden = [
        'post_status', 'post_date_gmt',
        'post_modified_gmt', 'ping_status',
        'comment_status', 'post_content',
        'post_excerpt', 'post_password',
        'to_ping', 'pinged',
        'post_content_filtered', 'post_parent',
        'menu_order', 'comment_count',
        'post_author', 'ID', 'post_title', 'post_name',
        'post_type', 'post_date', 'post_modified'
    ];

    protected $attributes = [
        'post_status' => 'inherit',
        'ping_status' => 'closed',
        'post_type' => 'attachment'
    ];

    public function user()
    {
        return $this->belongsTo(Avatar::class, 'ID', 'post_author');
    }

    public function newQuery()
    {
        return parent::newQuery()
            ->wherePostType(static::TYPE)
            ->wherePostStatus(static::STATUS)
            ->where('post_parent', '>', 0);
    }
}
