<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
use Jenssegers\Agent\Agent;

Route::get('auth/{provider}/token/{token}', function () {
    return response()->json(request()->route('token'));
})->name('token');
Route::get('auth/{provider}', 'AuthController@redirect');
Route::get('auth/{provider}/callback', 'AuthController@callback');


Route::get('share', function (){
    $agent = new Agent();
    if ($agent->is('iPhone') || $agent->is('OS X')) {
        return redirect()->to('https://www.apple.com/ru/ios/app-store/');
    } elseif ($agent->isAndroidOS()) {
        return redirect()->to('https://play.google.com/store/apps/details?id=kz.avsoft.neupusti');
    } else {
        return redirect()->to('http://neupusti.net/');
    }
});