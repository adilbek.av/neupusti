<?php

use Illuminate\Http\Request;
\Carbon\Carbon::setLocale('ru');
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'auth'
], function () {

    Route::post('login', 'AuthController@login');
    Route::post('register', 'AuthController@register');
    Route::post('edit', 'AuthController@edit');
    Route::post('recover', 'AuthController@recover');
    Route::post('logout', 'AuthController@logout');
    Route::post('refresh', 'AuthController@refresh');
    Route::post('me', 'AuthController@me');
    Route::post('password/email', 'Auth\ForgotPasswordController@getResetCode');
//    Route::post('password/code', 'Auth\ForgotPasswordController@verifyResetCode');
//    Route::post('password/reset', 'Auth\ResetPasswordController@reset');

    Route::post('avatar', 'AuthController@changeAvatar');
    Route::post('password/change', 'AuthController@changePassword');

    Route::post('{provider}', 'AuthController@oauth');
    
});


Route::get('test', function (){
    return \App\Tarif::get();
});

Route::get('purchase', function (){
    return response()->json(DB::table('purchase')->first());
});

Route::resource('posts', 'PostController')->only('index', 'show', 'update');
Route::resource('tarifs', 'TarifController')->only('index');
Route::get('oplata', 'TarifController@show');
Route::get('tarifs/{level}', 'TarifController@showTarif');
Route::post('notifications/{post}', 'PostController@addToNotification');
Route::resource('menus', 'MenuController')->only('index', 'show');
Route::resource('categories', 'CategoryController')->only('index', 'show');
Route::get('countries', 'CategoryController@countries');
Route::post('emails/feedback', 'EmailController@sendFeedback');
Route::resource('comments', 'CommentController')->only('index', 'store', 'show');

Route::post('notify', 'PostController@notify');
Route::post('register-device', 'DeviceController@registerDevice');
Route::get('nots', 'DeviceController@nots');
