<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Языковые ресурсы напоминания пароля
    |--------------------------------------------------------------------------
    |
    | Последующие языковые строки возвращаются брокером паролей на неудачные
    | попытки обновления пароля в таких случаях, как ошибочный код сброса
    | пароля или неверный новый пароль.
    |
    */

    'password' => 'Пароль должен быть не менее шести символов и совпадать с подтверждением.',
    'reset'    => 'Ваш пароль был сброшен!',
    'no_reset'    => 'Произошла ошибка. Ваш пароль не был сброшен!',
    'sent'     => 'Код на сброс пароля был отправлен!',
    'token'    => 'Ошибочный код сброса пароля.',
    'user'     => 'Не удалось найти пользователя с указанным электронным адресом.',
];
