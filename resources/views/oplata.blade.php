<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Neupusti.kz</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <script type='text/javascript' src='https://neupusti.net/wp-includes/js/jquery/jquery.js?ver=1.12.4'></script>
    <script src="https://neupusti.net/wp-content/plugins/indeed-membership-pro/assets/js/functions.js"></script>

    <style>
        html, body {
            width: 100vw;
            height: 100vh;
            margin: 0;
            overflow: hidden;
        }

        .section-tarif {
            background-color: #f1f1f3;
            padding: 25px;
            text-align: center;
            margin: 15px;
            border-radius: 3px;
            position: relative;
            -webkit-box-shadow: 0 10px 6px -6px #6d6d6d;
            -moz-box-shadow: 0 10px 6px -6px #6d6d6d;
            box-shadow: 0 10px 6px -6px #6d6d6d;
        }

        .section-tarif h3 {
            color: #1A4E74;
            margin: 0 0 15px;
        }

        .section-tarif h3 span {
            color: green;
        }

        .section-tarif p {
            margin: 0;
            font-weight: normal;
            /*background-color: #f36510;*/
            color: #6d6d6d;
            display: inline-block;
            font-size: 18px;
            border-radius: 5px;
            /*background-image: -webkit-linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.1) 50%,rgba(0,0,0,.1));*/
            /*background-image: -o-linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.1) 50%,rgba(0,0,0,.1));*/
            /*background-image: linear-gradient(rgba(0,0,0,0),rgba(0,0,0,.1) 50%,rgba(0,0,0,.1));*/
            /*filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00000000', endColorstr='#1a000000', GradientType=0);*/
        }

        .checkout {
            margin: 15px 0;
        }

        .flex {
            display: flex;
            flex-direction: column;
        }

        .checkout input {
            margin-bottom: 15px;
            border-width: 0 0 1px 0;
            padding: 5px 15px;
            border-color: #6d6d6d;
        }

        .padding {
            padding: 0 15px;
        }

        #terms {
            display: none;
        }

        .methods {
            list-style: none;
            margin: 0;
            padding: 0 15px;
        }

        .methods li {
            color: #6d6d6d;
        }

        .methods li input {
            position: absolute;
            opacity: 0;
            pointer-events: none;
        }

        .input-radio:checked+label, .input-radio:not(:checked)+label {
            position: relative;
            padding-left: 35px;
            cursor: pointer;
            display: inline-block;
            height: 1.5625rem;
            line-height: 1.5625rem;
            -webkit-transition: .28s ease;
            -o-transition: .28s ease;
            transition: .28s ease;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .methods li label::before {
            border: 1px solid #1A4E74;
            border-radius: 50%;
            content: '';
            position: absolute;
            left: 0;
            top: 0;
            margin: 4px;
            width: 12px;
            height: 12px;
            z-index: 0;
            -webkit-transition: .28s ease;
            -o-transition: .28s ease;
            transition: .28s ease;
        }

        .methods li label::after {
            content: '';
            border: 1px solid #1A4E74;
            border-radius: 50%;
            position: absolute;
            left: 0;
            top: 0;
            margin: 4px;
            width: 12px;
            height: 12px;
            -webkit-transform: scale(1.02);
            -ms-transform: scale(1.02);
            transform: scale(1.02);

        }

        .input-radio:checked+label::after {
            background-color: #1A4E74;
        }

        .button {
            color: white;
            background-color: #1A4E74;
            display: block;
            width: 100%;
            padding: 15px !important;
            position: absolute;
            bottom: 0;
            margin: 0 !important;
            border: 0;
            font-size: 18px;
        }

        .button img {
            width: 28px;
            position: absolute;
            right: 25px;
            top: 50%;
            transform: translateY(-50%);
        }

        .blue {
            color: #1A4E74;
        }

        .scroll {
            overflow-y: scroll;
            max-height: calc(100vh - 90px);
        }
    </style>
</head>
<body>
<iframe src="https://neupusti.net/?add-to-cart={{ request('id') }}" style="display: none"></iframe>
<div>

    <form method="post" class="checkout" action="https://neupusti.net/checkout/" enctype="multipart/form-data">
        <div class="scroll">
            <section class="section-tarif">
                <h3>Стоимость подписки: <span>{{ $tarif->price }}KZT</span></h3>
                <p>Полный доступ к контенту: {{ $tarif->post_title }}</p>
            </section>
            <div class="flex">
                <p class="blue padding">Введите ваши данные:</p>
                <input type="email" class="input-text" name="billing_email" id="billing_email" placeholder="Email" value="{{ request('email') }}" autocomplete="email username" />
                <input type="hidden" class="input-text" name="account_username" id="account_username" placeholder="" value="{{ request('email') }}" autocomplete="email username" />
                <input type="text" class="input-text" name="billing_phone" id="billing_phone" placeholder="Номер телефона" value="{{ request('phone') }}" autocomplete="tel" />
                <input type="hidden" class="input-text" name="coupon_code" id="coupon_code" placeholder="" value=""/>
                <input type="password" class="input-text " name="account_password" id="account_password" placeholder="Пароль" value="{{ request('password') }}">
            </div>

            <div class="flex">
                <p class="blue padding">Выберите способ оплаты:</p>
                <ul class="wc_payment_methods payment_methods methods">
                    <li class="wc_payment_method payment_method_cp">
                        <input id="payment_method_cp" type="radio" class="input-radio" name="payment_method" value="cp" data-order_button_text="" /><label for="payment_method_cp">Банковская карта</label>
                    </li>
                    <li class="wc_payment_method payment_method_qiwi_provaider">
                        <input id="payment_method_qiwi_provaider" type="radio" class="input-radio" name="payment_method" value="qiwi_provaider" checked='checked' data-order_button_text="" /><label for="payment_method_qiwi_provaider">Qiwi Терминал </label>
                    </li>
                    <li class="wc_payment_method payment_method_qiwi_gateway">
                        <input id="payment_method_qiwi_gateway" type="radio" class="input-radio" name="payment_method" value="qiwi_gateway" data-order_button_text="" /><label for="payment_method_qiwi_gateway">Qiwi Visa Wallet </label>
                    </li>
                </ul>
            </div>
        </div>

        <div class="form-row place-order">

            <input type="checkbox" name="terms" id="terms" checked/>
            <input type="hidden" name="terms-field" value="1" />
            <button type="submit" class="button alt" name="woocommerce_checkout_place_order" id="place_order">
                Оплатить <img src="{{ asset('arrow.png') }}" alt=""></button>
            <input type="hidden" id="_wpnonce" name="_wpnonce" value="9505802644" />
            <input type="hidden" name="_wp_http_referer" value="/checkout/" />
        </div>
    </form>
</div>
</body>
</html>
